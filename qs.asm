;;; Quick-sort em assembly

;;; Rafael L. L. Coelho, João Victor M. Carneiro

; TODO:
; Concluir implementacao da particao
; Concluir implementacao do quicksort
; Testar funcionamento total
; Substituir loops condicionais por funcoes no resto do codigo

section .data
erromsg:       db "Erro de entrada", 10  ; mensagem de erro e nova linha
errolen:       equ $ - erromsg           ; tamanho da mensagem

section .bss
input:     resb 255        ; input de usuarios
inputmax:  equ $ - input   ; tamanho max para input
inputlen:  resd 1          ; real tamanho do input (nao inicializado)
array:     resb 255        ; vetor de inteiros
arraymax:  equ $ - array   ; tamanho max da array
arraylen:  resd 1          ; real tamanho do vetor (nao inicializado)
result:    resb 255        ; vetor convertido para char
resultmax: equ $ - result  ; tamanho maximo do resultado


section .text
global main
main:

le_input:
; leitura da lista de numeros atraves do stdin
    mov eax, 3          ; syscall 3 para leitura
    mov ebx, 0          ; buffer 0 stdin
    mov ecx, input      ; endereco de destino
    mov edx, inputmax   ; tamanho do buffer que recebe a string int 80h           
    int 80h             ; interrupcao para syscall

pre_char_int:
; marca fim de input com 0 e prepara ponteiros
; faz uso de eax que guarda o tamanho do input
    mov [inputlen], eax     ; salva tamanho real do input
    mov esi, input          ; ESI = ponteiro para comeco da string
    mov edi, array          ; EDI = ponteiro para comeco da array
    mov ecx, 0              ; ECX = tamanho da array
    mov edx, 0              ; EDX = flag de estado usada na conversao

char_para_int:
; converte chars de -9 a 9 para inteiros e os armazena na array
; incrementando a contagem de elementos do vetor
; utiliza flag em EDX para saber em que estado se encontra:
;    0 - flag inicial ou apos espaco encontrado
;    1 - flag apos sinal negativo encontrado
;    2 - flag apos numero encontrado
    xor eax, eax            ; limpa eax
    mov al, [esi]           ; AL char sob o ponteiro atual

    ; regras de comparacao:
    ;    1 - se for fim de string encerra conversao
    ;    2 - se for espaco avanca
    ;    3 - se encontrar um '-' nega o numero seguinte
    ;    4 - se for numeral converte para int e armazena
    ;    e1 - se for qualquer outra coisa da erro
    cmp al, 10
        je pre_quicksort
    cmp al, " "
        je espaco
    cmp al, "-"
        je negativo
    cmp al, "1"
        je numero
    cmp al, "2"
        je numero
    cmp al, "3"
        je numero
    cmp al, "4"
        je numero
    cmp al, "5"
        je numero
    cmp al, "6"
        je numero
    cmp al, "7"
        je numero
    cmp al, "8"
        je numero
    cmp al, "9"
        je numero
    cmp al, "0"
        je numero
    jmp erro

    espaco:
    ; precedido por numero, avanca para proximo char
    ; precedido por espaco ou negativo, retorna erro
    cmp edx, 0        ; confere se anterior foi espaco
        je erro       ; se sim, erro
    cmp edx, 1        ; confere se anterior foi negativo
        je erro       ; se sim, erro
    inc esi           ; move ponteiro para proximo char na string
    mov edx, 0        ; marca flag com espaco
    jmp char_para_int ; retorna ao loop

    negativo:
    ; precedido por negativo ou numero, retorna erro
    ; precedido por espaco, marca flag e avanca
    cmp edx, 1        ; confere se anterior foi negativo
        je erro       ; se sim, erro
    cmp edx, 2        ; confere se anterior foi numero
        je erro       ; se sim, erro
    inc esi           ; move ponteiro para proximo char na string
    mov edx, 1        ; marca flag com negativo
    jmp char_para_int ; retorna ao loop

    numero:
    ; precedido por espaco, converte e armazena
    ; precedido por negativo, converte, nega e armazena
    ; precedido por numero, retorna erro
    cmp edx, 2        ; confere se anterior foi numero
        je erro       ; se sim, erro
    sub al, 48        ; subtrai 48 do char, equivalente em int
    cmp edx, 0        ; confere se anterior foi espaco
        je armazena   ; se for positivo, pula a negacao do int
    neg al            ; nega o int em ebx

        armazena:
        ; move int e tamanho em memoria e ajusta ponteiros
        mov edx, 2            ; marca flag com numero
        mov byte [edi], al    ; move int para endereco em EDI
        inc esi               ; avanca ponteiro da string
        inc edi               ; avanca ponteiro do vetor
        inc ecx               ; incrementa tamanho do vetor
        mov [arraylen], ecx   ; salva tamanho real do input
        jmp char_para_int     ; retorna ao loop

pre_quicksort:
; prepara a passagem de parametros para a funcao de quicksort
; sao parametros um ponteiro para o vetor, menor posicao e maior posicao
    mov ecx, [arraylen]       ; armazena tamanho do vetor
    dec ecx                   ; ECX = posicao do ultimo elemento
    push array                ; empilha ponteiro para inicio do vetor
    push 0                    ; empilha posicao do primeiro elemento
    push ecx                  ; empilha posicao do ultimo elemento
    push 0                    ; empilha espaco reservado para guardar pivo
    call quicksort            ; chama funcao

pre_int_char:
; prepara registradores para conversao de int para char
    mov esi, array           ; ESI = ponteiro para posicao atual do vetor
    mov edi, result          ; EDI = ponteiro para posicao atual do resultado
    mov ecx, [arraylen]      ; ECX = tamanho do vetor de inteiros
    mov edx, 0               ; EDX = contagem de numeros na resposta

int_para_char:
; converte vetor de inteiros para string imprimivel
; soma 48  a cada um e confere se 
    xor eax, eax             ; limpa eax
    cmp edx, ecx             ; confere se o resultado ja tem todos os numeros
    je imprime               ; se sim, imprimer resultado
    mov al, [esi]            ; EAX = int na posicao atual do vetor
    cmp al, 0                ; compara conteudo atual do vetor com 0
    jge conv_char            ; se for maior que 0, pula char negativo

    nega_char:
    ; imprime o sinal negativo e nega int atual
    mov byte [edi], '-'      ; salva char negativo na string resultante
    neg al                   ; torna inteiro positivo
    inc edi                  ; avanca ponteiro da string resultante

    conv_char:
    ; converte int positivo diretamente para char
    add al, 48               ; transforma em correspondente ascii
    mov [edi], al            ; salva char na string resultante
    inc edi                  ; avanca ponteiro da string um endereco
    mov byte [edi], ' '      ; imprime espaco entre numeros
    inc edi                  ; avanca ponteiro string mais uma vez
    inc esi                  ; avanca ponteiro do vetor
    inc edx                  ; aumenta contagem de numeros no resultado
    jmp int_para_char        ; retorna ao inicio do loop de conversao

imprime:
; imprime string resultante com dados ordenados
    dec edi                      ; aponta para ' ' no fim da string
    mov byte [edi], 10           ; insere '\n' ao final da string
    mov eax, 4                   ; syscall 4 para escrita
    mov ebx, 1                   ; buffer 1 stdout
    mov ecx, result              ; ponteiro para o dado a ser lido
    mov edx, [inputlen]          ; tamanho da string resultado igual ao input
    int 80h                      ; interrupcao para syscall

sair:
; saida de programa unix
    mov eax, 1                   ; syscall 1 para sair
    mov ebx, 0                   ; erro 0
    int 80h                      ; interrupcao para syscall

erro:
; imprime aviso de erro ao console
    mov eax, 4               ; syscall 4 para escrita
    mov ebx, 1               ; buffer 1 stdout
    mov ecx, erromsg         ; ponteiro para memoria a ser lida
    mov edx, errolen         ; tamanho do dado
    int 80h                  ; interrupcao para syscall
    jmp sair                 ; vai para saida do programa

quicksort:
; implementacao quicksort que usa ultimo elemento como pivo para ordenacao
; faz uso de recursividade, dividindo e conquistando atraves de chamadas
; a funcao particiona e a propria quicksort
    push ebp             ; salva ebp na pilha
    mov ebp, esp         ; nova base sera onde era o topo
    sub esp, 16          ; decrementa topo para 4 variaveis locais

    mov eax, [ebp + 20]  ; EAX = ponteiro para o vetor
    mov ebx, [ebp + 16]  ; EBX = low, primeiro elemento do vetor de inteiros
    mov ecx, [ebp + 12]  ; ECX = high, ultimo elemento do vetor de inteiros
    mov esi, [ebp + 8]   ; ESI = posicao atual do pivo

    cmp ebx, ecx         ; compara primeiro e ultimo elementos              
    jge ret_qs           ; caso low >= high, retorna a funcao 

    mov [ebp-4], eax   ; salva ponteiro como parametro 1
    mov [ebp-8], ebx   ; salva low como parametro 2
    mov [ebp-12], ecx  ; salva high como parametro 3
    mov [ebp-16], esi  ; salva posicao do pivo
    call particiona    ; chamada da funcao de particionamento
    mov [ebp+8], esi   ; retorna posicao do ultimo pivo 

    mov esi, [ebp+8]   ; restaura posicao do ultimo pivo 
    mov ecx, [ebp+12]  ; restaura high
    mov ebx, [ebp+16]  ; restaura low
    mov eax, [ebp+20]  ; restaura ponteiro para o vetor
    mov edi, esi       ; move pivo para reg edi
    dec edi            ; aponta para o elemento a esquerda do pivo
    mov [ebp-4], eax   ; salva ponteiro como parametro 1
    mov [ebp-8], ebx   ; salva low como parametro 2
    mov [ebp-12], edi  ; salva elemento a esqueda do pivo como parametro 3
    mov [ebp-16], esi  ; salva posicao do pivo
    call quicksort     ; chamada recursiva que explora a particao à esquerda

    mov esi, [ebp+8]   ; restaura posicao do ultimo pivo 
    mov ecx, [ebp+12]  ; restaura high
    mov ebx, [ebp+16]  ; restaura low
    mov eax, [ebp+20]  ; restaura ponteiro para o vetor
    mov edi, esi       ; move pivo para reg edi
    inc edi            ; aponta para o elemento a direita do pivo
    mov [ebp-4], eax   ; salva ponteiro como parametro 1
    mov [ebp-8], edi   ; salva elemento a direita do pivo como parametro 2
    mov [ebp-12], ecx  ; salva high como parametro 3
    mov [ebp-16], esi  ; salva posicao do pivo
    call quicksort     ; chamada recursiva que explora a particao à direita
    
    ret_qs:
    ; retorna da funcao quicksort
    mov esp, ebp         ; remove variaveis da pilha
    pop ebp              ; recupera valor de ebp
    ret                  ; retorna da funcao

particiona:
; coloca o pivo (high) em sua posicao correta, com todos elementos menores
; a sua esquerda e todos os maiores a sua direita
    push ebp             ; salva ebp na pilha
    mov ebp, esp         ; nova base sera onde era o topo
    mov ecx, [ebp+12]    ; ECX = high, ultimo elemento do vetor de inteiros
    mov ebx, [ebp+16]    ; EBX = low, primeiro elemento do vetor de inteiros
    mov eax, [ebp+20]    ; EAX = ponteiro para o vetor

    mov dl, [eax+ecx]    ; EDX = pivo, ultimo elemento do vetor
    mov esi, ebx         ; ESI = i, limiar dos elementos menores que o pivo
    dec esi              ; esi comeca apontando para fora do vetor
    mov edi, ebx         ; EDI = j, endereco do elemento atual sendo comparado

    loop_part:
    ; procura por elementos menores ou iguais ao pivo e o jogam a esquerda de i
	push ebx                     ; salva valor de low
	xor ebx, ebx                 ; limpa ebx
	mov bl, [eax+edi]            ; armazena array[j] em reg
        cmp bl,  dl                  ; compara array[j] com array[pivo]
            jg incrementa_loop       ; se maior que, incrementa o loop

        push edx                     ; salva valor de edx
        xor edx, edx                 ; limpa edx para uso temporario
        inc esi                      ; i++
        mov dl, [eax+esi]            ; armazena array[i] em reg
        mov [eax+esi], bl            ; array[i] = array[j]
        mov [eax+edi], dl            ; array[j] = array[i]
	pop edx                      ; restaura edx

        incrementa_loop:
	; incrementa valores para dar continuidade ao loop
	pop ebx                      ; restaura low em ebx
        inc edi                      ; j++
        cmp edi, ecx                 ; compara j com high 
        jl loop_part                 ; se j < high volta ao loop
    
    ; troca as posicoes do elemento posterior a i e o pivo
    ; isso garante que o pivo esteja no posicao correta
    ; com elementos menores que ele a esquerda e maiores a direita
    push ebx                         ; salva low temporariamente
    push edx                         ; salva pivo temporariamente
    xor ebx, ebx                     ; limpa ebx para uso temporario
    xor edx, edx                     ; limpa edx para uso temporario
    mov bl, [eax+ecx]                ; armazena array[high] em reg
    mov dl, [eax+esi+1]              ; armazena array[i+1] em reg
    mov [eax+esi+1], bl              ; array[i+1] = array[high]
    mov [eax + ecx], dl              ; array[high] = array[i+1]
    pop edx                          ; restaura pivo em edx
    pop ebx                          ; restaura low em ebx
    
    inc esi                          ; indica a posicao atual do pivo
    pop ebp                          ; retorna valor de ebp
    ret                              ; retorna a execucao anterior

